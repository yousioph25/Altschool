# Altschool Cloud Assignment
## Exercise 1
### Setup Ubuntu 20.04 LTS on your local machine using Vagrant
Result shown after running **ifconfig** command on git-bash 
![**ifconfig** result on terminal](Exercise1.png.png)


## Exercise 2
### Linux commands excluding those found in the video
1. more  
   Display content of a file page-by-page.
   ![*more* result on terminal](more.png) 
2. less  
    Displays contents of a file one page at a time. It’s advanced than more command.
    ![*less* result on terminal](less.png)
3. sed  
   Stream editor for filtering and transforming text (from a file or a pipe input).
   ![*sed* result on teerminal](sed.png)
4. nl  
   Show numbered line while displaying the contents of a file.
   ![*nl* result on terminal](nl.png)
5. find  
    Do a file search in a directory hierarchy.
    ![*find* result on terminal](find.png)
6. history  
   Shows the command history.
   ![*history* result on terminal](history.png)
7. locate  
    Used to find files by their name.
    ![*locate* result on terminal](locate.png)
8. whereis  
    Locate the binary, source, and man page files for a command.
    ![**whereis** result on terminal](whereis%20and%20which.png)
9.  which  
    For a given command, lists the pathnames for the files which would be executed when the command runs.
    ![**which** result on terminal](whereis%20and%20which.png)
10. cal:  
    It shows calendar highlighting the date
    ![**cal** result on terminal](cal.png)


## Exercise 3
* Create 3 groups – admin, support & engineering and add the admin group to sudoers. 
* Create a user in each of the groups. 
* Generate SSH keys for the user in the admin group  

Content of /etc/passwd  
![Content of /etc/passwd](Passwd.png)

Content of /etc/group  
![Content of /etc/group](Assignment%20group.png)

Content of /etc/sudoers  
![Content of /etc/sudoers](sudoers.png)


## Exercise 4
* Install PHP 7.4 on your local linux machine using the ppa:ondrej/php package repo.
content of /etc/apt/sources.list and the output of php -v command  
![content of /etc/apt/sources.list](PPA.png)  
![output of php -v command](php.png)  


## Exercise 5  
* You already have Github account, aso setup a GitLab account if you don’t have one already
* You already have a altschool-cloud-exercises project, clone the project to your local system
* Setup your name and email in Git’s global config  

Result of git config -l  ![Result of git config -l](git%20config-i.png)  
git remote -v
git log